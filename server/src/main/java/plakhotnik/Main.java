package plakhotnik;

import plakhotnik.server.Server;

import java.io.IOException;



public class Main {

    public static void main(String[] args) throws IOException {

        Server server = new Server(8008);
        System.out.println("Server start");
        server.listen();
    }

}
