package plakhotnik.server;

import java.util.HashMap;
import java.util.Map;

public class InfoOfConnectUsers {

    private Map<MessengerThread, String> infoMap = new HashMap<>();

    public Map<MessengerThread, String> getInfoMap() {
        return this.infoMap;
    }

    public String showUsers() {
        StringBuilder stringBuilder = new StringBuilder();
        for (Map.Entry<MessengerThread, String> info : infoMap.entrySet()) {
            stringBuilder.append(info.getValue()).append("; ");
        }
        return stringBuilder.toString();
    }

    public void addUserToMap(MessengerThread messengerThread, String name) {
        infoMap.put(messengerThread, name);
    }
}
