package plakhotnik.server;

import java.util.ArrayList;
import java.util.List;

public class InformationOfNotConnectUsers {

    private List<MessengerThread> usersThread = new ArrayList<>();

    public List<MessengerThread> getUsersThread() {
        return this.usersThread;
    }

    public void addMessengerThreadToList(MessengerThread messengerThread) {
        usersThread.add(messengerThread);
    }

    public void deleteMessengerThreadFromList(MessengerThread messengerThread) {
        usersThread.remove(messengerThread);
    }

}
