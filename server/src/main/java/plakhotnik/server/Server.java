package plakhotnik.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {

    private final ServerSocket serverSocket;

    // Map with info about users
    public InfoOfConnectUsers infoOfConnectUsers = new InfoOfConnectUsers();

    public InformationOfNotConnectUsers infoUsersList = new InformationOfNotConnectUsers();

    public Server(int port) throws IOException {
        this.serverSocket = new ServerSocket(port);
    }

    public void listen() throws IOException {

        while (true) {
            System.out.println("Waiting...");
            Socket clienSocket = this.serverSocket.accept();
            System.out.println("New person is connected");

            // create thread for each connected client
            MessengerThread messengerThread = new MessengerThread(clienSocket,
                    infoUsersList, infoOfConnectUsers);

            infoUsersList.addMessengerThreadToList(messengerThread);

            messengerThread.start();
        }
    }
}
