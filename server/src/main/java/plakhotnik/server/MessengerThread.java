package plakhotnik.server;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.util.Map;

public class MessengerThread extends Thread {

    private Socket clienSocket;

    private BufferedWriter writer;

    private BufferedReader reader;

    private String name;

    private InformationOfNotConnectUsers infoUsersList;

    private InfoOfConnectUsers infoOfConnectUsers;

    public MessengerThread(Socket clientSocket, InformationOfNotConnectUsers infoUsersList,
                           InfoOfConnectUsers infoOfConnectUsers) throws IOException {
        this.clienSocket = clientSocket;
        this.infoUsersList = infoUsersList;
        this.infoOfConnectUsers = infoOfConnectUsers;
        this.reader = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
        this.writer = new BufferedWriter(new OutputStreamWriter(clientSocket.getOutputStream()));
    }

    @Override
    public void run() {
        try {
            writer.write("Please, type your user name\n");
            writer.flush();
            name = this.reader.readLine();
            infoOfConnectUsers.addUserToMap(this, name);

            for (Map.Entry<MessengerThread, String> info : infoOfConnectUsers.getInfoMap().entrySet()) {
                MessengerThread mt = info.getKey();
                mt.sendMessage(name + " is connected to this chat");
            }

            while (clienSocket.isConnected()) {
                String message = this.reader.readLine();

                if (message.equals("quit")) {
                    writer.write("GoodBye");
                    clienSocket.close();
                    // remove from list
                    infoUsersList.deleteMessengerThreadFromList(this);
                    for (Map.Entry<MessengerThread, String> info : infoOfConnectUsers.getInfoMap().entrySet()) {
                        MessengerThread mt = info.getKey();
                        mt.sendMessage(name + " left chat");
                    }
                    break;
                }

                if (message.equals("show users")) {
                    writer.write(infoOfConnectUsers.showUsers());
                    writer.flush();
                }

                // send message for all users who connected
                if (!message.equals("show users")) {
                    for (Map.Entry<MessengerThread, String> info : infoOfConnectUsers.getInfoMap().entrySet()) {
                        MessengerThread mt = info.getKey();
                        mt.sendMessage(name + ": " + message);
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            // remove from list
            infoUsersList.deleteMessengerThreadFromList(this);
            for (Map.Entry<MessengerThread, String> info : infoOfConnectUsers.getInfoMap().entrySet()) {
                MessengerThread mt = info.getKey();
                try {
                    mt.sendMessage(name + " left chat");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void sendMessage(String message) throws IOException {
        writer.write(message.concat("\n"));
        writer.flush();
    }

}
