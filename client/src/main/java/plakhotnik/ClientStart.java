package plakhotnik;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ClientStart {
    private BufferedReader consoleReader = new BufferedReader(new InputStreamReader(System.in));
    private String ip;
    private int port;

    public void start() throws IOException {
        getIp();
        getPort();

        try (Client client = new Client(ip, port)) {
            client.start();

            while (true) {
                String clientMessege = consoleReader.readLine();
                client.sendMessege(clientMessege);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getIp() throws IOException {
        System.out.println("Please, type server ip: ");
        this.ip = consoleReader.readLine();
        return ip;
    }

    public int getPort() throws IOException {
        System.out.println("Please, type server port: ");
        this.port =  Integer.parseInt(consoleReader.readLine());
        return port;
    }
}
