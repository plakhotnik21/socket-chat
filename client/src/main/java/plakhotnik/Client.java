package plakhotnik;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;

public class Client extends Thread implements Closeable {

    private Socket socket;
    private BufferedReader bufferedReader;
    private BufferedWriter bufferedWriter;

    public Client(String ip, int port) throws IOException {
        this.socket = new Socket(ip, port);
        this.bufferedReader = new BufferedReader(new InputStreamReader(this.socket.getInputStream()));
        this.bufferedWriter = new BufferedWriter(new OutputStreamWriter(this.socket.getOutputStream()));
    }

    @Override
    public void run() {
        while (true) {
            try {
                String messege = getMessege();
                System.out.println(messege);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void sendMessege(String messege) throws IOException {
        if (messege.equals("quit")) {
            close();
        } else {
            this.bufferedWriter.write((messege).concat("\n"));
            bufferedWriter.flush();
        }
    }

    public String getMessege() throws IOException {
        return this.bufferedReader.readLine();
    }

    @Override
    public void close() {
        try {
            this.bufferedWriter.close();
        } catch (IOException ignored) { }
        try {
            this.bufferedReader.close();
        } catch (IOException ignored) { }
        try {
            this.socket.close();
        } catch (IOException ignored) { }
    }

}
